package com.packagegenerator.core.schedulers;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.vault.fs.api.PathFilterSet;
import org.apache.jackrabbit.vault.fs.api.ProgressTrackerListener;
import org.apache.jackrabbit.vault.fs.config.DefaultWorkspaceFilter;
import org.apache.jackrabbit.vault.packaging.JcrPackage;
import org.apache.jackrabbit.vault.packaging.JcrPackageDefinition;
import org.apache.jackrabbit.vault.packaging.JcrPackageManager;
import org.apache.jackrabbit.vault.packaging.Packaging;
import org.apache.jackrabbit.vault.util.DefaultProgressListener;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, metatype = true)
@Service(GeneratePackageScheduledTask.class)

public class GeneratePackageScheduledTask {

		
	@Reference
	private SlingRepository repository;
	
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	@Reference
	private SlingSettingsService settingsService;
	
	@Reference
	private Scheduler  scheduler;
	
	@Reference
	private Packaging packaging;
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference(cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, referenceInterface = GeneratePackageSchedulerRequest.class, policy = ReferencePolicy.DYNAMIC)
	private final List<GeneratePackageSchedulerRequest> providers = new LinkedList<GeneratePackageSchedulerRequest>();

	protected void bindProviders(final GeneratePackageSchedulerRequest config) throws Exception {

		providers.add(config);
		
		final String schedulingExpression=config.getSheduleExpression();
		final String jobname= config.getJobname();
		final String[] packageFilters = config.getPackageFilters();		
				
		
		final Runnable job = new Runnable() {
			public void run() {	
				
				Session session=null;
				if (isRunMode("author")&& isMasterRepository()) {

					try {
						
						 Map<String, Object> param = new HashMap<String, Object>();
						 param.put(ResourceResolverFactory.SUBSERVICE, "packageService");
						 
						 ResourceResolver resolver = resolverFactory.getServiceResourceResolver(param);
						
						//ResourceResolver resolver = resolverFactory.getAdministrativeResourceResolver(null);
						session= resolver.adaptTo(Session.class);
						
						logger.debug("{} is now running, Parameter='{}'",jobname, packageFilters);						 
						
						 List<PathFilterSet> pathFilterSets = new ArrayList<PathFilterSet>();
					     for (String packageFilter : packageFilters) {
					    	 pathFilterSets.add(new PathFilterSet(packageFilter));
					     }
					     
					     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
					        
					    JcrPackageManager jcrPackageManager = packaging.getPackageManager(session);

						JcrPackage jcrPackage = jcrPackageManager.create("ScheduledPackages", jobname+sdf.format(new Date()).toString(), "1.0");
						JcrPackageDefinition jcrPackageDefinition = jcrPackage.getDefinition();
						DefaultWorkspaceFilter workspaceFilter = new DefaultWorkspaceFilter();
								
						for (PathFilterSet pathFilterSet : pathFilterSets) {
							workspaceFilter.add(pathFilterSet);
						}

						jcrPackageDefinition.setFilter(workspaceFilter, true);					
						jcrPackageDefinition.set(JcrPackageDefinition.PN_DESCRIPTION, "Scheduled Package"+jobname+sdf.format(new Date()).toString(), false);
						String filePath=config.getRootPath()+File.separator+ config.getJobname()+sdf.format(new Date()).toString()+".zip";
						logger.debug("Package Export File Path:"+filePath);
						
						ProgressTrackerListener listener = new DefaultProgressListener();
						jcrPackageManager.assemble(jcrPackageDefinition,listener, new FileOutputStream(filePath));
						
						jcrPackageManager.remove(jcrPackage);
												
					} catch (Exception rex) {
						rex.printStackTrace();
						logger.error("Error occurred in pckage creation..", rex.getMessage());
					}finally {				
						
						if (session != null) {
							session.logout();
						}						
					}					
				}

				logger.debug("run() END"+jobname);				
			}

		};	
		
		ScheduleOptions so = scheduler.EXPR(schedulingExpression);		
		so.name(jobname);
		this.scheduler.schedule(job, so);	
		logger.debug("Scheduled Job: " + config.getJobname()+" "+schedulingExpression);
	}

	protected void unbindProviders(final GeneratePackageSchedulerRequest config) {		
		logger.debug("Removed Job: " + config.getJobname());		
		this.scheduler.unschedule(config.getJobname());	
		providers.remove(config);		
	}
	
	private Boolean isRunMode(String mode) {
		Set<String> runModes = settingsService.getRunModes();
		for (String runMode : runModes) {
			if (runMode.equalsIgnoreCase(mode)) {
				logger.debug("Current Runmode is : " + runMode);
				return true;
			}
		}
		return false;
	}
	
	public boolean isMasterRepository(){
	    final String isMaster = repository.getDescriptor("crx.cluster.master");
	    logger.debug("isMaster.."+isMaster);
	    return isMaster!=null && !isMaster.equals("") && Boolean.parseBoolean(isMaster);
	}
	
}

