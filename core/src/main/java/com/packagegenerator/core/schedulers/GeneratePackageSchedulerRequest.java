package com.packagegenerator.core.schedulers;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(configurationFactory = true, policy = ConfigurationPolicy.OPTIONAL, metatype = true, immediate = true, label = "Scheduled Package Generator")
@Service(value = GeneratePackageSchedulerRequest.class)
public class GeneratePackageSchedulerRequest {
	
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Property(unbounded=PropertyUnbounded.DEFAULT, label="Scheduler Expression", description="Scheduler Expression", value="")
	private static final String SCHEDULER_EXPRESSION = "sheduleExpression";
	private String sheduleExpression;
	
	@Property(unbounded = PropertyUnbounded.ARRAY, label = "Packages Filter String", cardinality = 50, description = "Packages Filter String")
	public static final String PACKAGE_FILTERS = "packageFilters";
	private String[] packageFilters;
	
	@Property(unbounded=PropertyUnbounded.DEFAULT, label="Package Name", description="Package Name")
	private static final String PACKAGE_NAME = "packgeName";
	private String packageName;
	
	@Property(unbounded=PropertyUnbounded.DEFAULT, label="Root Path to store the package", description="Root Path")
	private static final String ROOT_PATH = "rootpath";
	private String rootPath;
	
	
	@Activate
	protected void activate(final ComponentContext ctx) {		

		Dictionary<?, ?> props = ctx.getProperties();
		sheduleExpression = PropertiesUtil.toString(props.get(SCHEDULER_EXPRESSION), "");
		
		packageFilters = PropertiesUtil.toStringArray(props.get(PACKAGE_FILTERS), null);
		packageName = PropertiesUtil.toString(props.get(PACKAGE_NAME), null);
		rootPath=PropertiesUtil.toString(props.get(ROOT_PATH), null);		
	}
	
	public String[] getPackageFilters() {
		return packageFilters;
	}

	public String getJobname() {
		return packageName;
	}
	
	public String getSheduleExpression() {
		return sheduleExpression;
	}

	public String getRootPath() {
		return rootPath;
	}

}
